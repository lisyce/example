Everything in this project should be mostly set up so that it requires minimal effort to start a new robot project using the gradle-rio

1) Clone https://gitlab.com/tahoma-robotics/bear-essentials then run "mvn clean install"

2) Clone this project, it should be a gradle project when opened with intellij, if not, fix it

3) Follow the TODOs in build.gradle to make sure dependencies are up to date

4) Start programming, Good Luck
