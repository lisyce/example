package org.tahomarobotics.robot.example;

import edu.wpi.first.wpilibj2.command.CommandBase;
import org.tahomarobotics.robot.oi.OI;

public class ExampleCommand extends CommandBase {

    private final Example example = Example.getInstance();
    private final OI oi = OI.getInstance();

    public ExampleCommand() {
        addRequirements(example);
    }

    @Override
    public void execute() {
        example.setExampleMotorOne(oi.getDriveRightStick());
        example.setExampleMotorTwo(oi.getDriveLeftTrigger());
    }

    @Override
    public void end(boolean interrupted) {
        example.setExampleMotorOne(0);
        example.setExampleMotorTwo(0);
    }

}
