package org.tahomarobotics.robot.example;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import org.tahomarobotics.robot.RobotMap;

public class Example extends SubsystemBase {

    private static final Example INSTANCE = new Example();

    private final CANSparkMax exampleMotorOne = new CANSparkMax(RobotMap.EXAMPLE_MOTOR_ONE, CANSparkMaxLowLevel.MotorType.kBrushless);
    private final CANSparkMax exampleMotorTwo = new CANSparkMax(RobotMap.EXAMPLE_MOTOR_TWO, CANSparkMaxLowLevel.MotorType.kBrushless);

    public static Example getInstance() {
        return INSTANCE;
    }

    private Example() {
        exampleMotorOne.setInverted(false);
        exampleMotorTwo.setInverted(false);

//        exampleMotorTwo.follow(exampleMotorOne);
    }

    @Override
    public void periodic() {

    }

    public void setExampleMotorOne(double power) {
        exampleMotorOne.set(power);
    }

    public void setExampleMotorTwo(double power) {
        exampleMotorTwo.set(power);
    }


}
