package org.tahomarobotics.robot.oi;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;

public class OI {

    private static final OI INSTANCE = new OI();
    private static final double DEADBAND = 5d / 127;

    public static OI getInstance() {
        return INSTANCE;
    }

    private XboxController driveController = new XboxController(0);

    private OI() {

    }

    public double getDriveRightStick() {
        return calcDeadband(driveController.getY(GenericHID.Hand.kRight));
    }

    public double getDriveLeftTrigger() {
        return driveController.getTriggerAxis(GenericHID.Hand.kLeft);
    }

    private double calcDeadband(double input) {
        if(Math.abs(input) > DEADBAND) return input;
        return 0;
    }

}
